#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <unistd.h>
#include <ctype.h>

#define ACCOUNT_COUNT 5
#define MAX_OPERATION_VALUE 500 //operations performed in range from -MAX_OP... to +MAX_OP...
#define MAX_INITIAL_VALUE 500   //each account at the beginning owns a value in range from 0 to MAX_INI...
#define THREAD_COUNT 7
#define OPERATION_COUNT 1
#define OPERATION_REPEATS 500
#define MUTEX_RELOCK_DELAY_US 5000

pthread_t threads[ACCOUNT_COUNT];
int account_value[ACCOUNT_COUNT];
sem_t semaphores[ACCOUNT_COUNT];        //account value, 'atomic' int
pthread_mutex_t mutexes[ACCOUNT_COUNT]; //is account currently used
unsigned int rand_seed[ACCOUNT_COUNT];

enum Options
{
    CHANGE,
    MOVE
};

volatile int value = 0;

void *worker_thread(void *arg)
{
    int id = (long)arg;

    printf("Thread %d joined\n", id);

    //prepare all (also unnecessary) informations
    int option = rand_r(&rand_seed[id]) % (MOVE + 1);
    int source = rand_r(&rand_seed[id]) % ACCOUNT_COUNT;
    int target = rand_r(&rand_seed[id]) % ACCOUNT_COUNT;
    int value = (rand_r(&rand_seed[id]) % (2 * MAX_OPERATION_VALUE)) - MAX_OPERATION_VALUE;
    int count = (rand_r(&rand_seed[id]) % OPERATION_COUNT) + 1;

    int absValue = value >= 0 ? value : -value;
    uint8_t isNegative = value < 0;

    for (int i = 0; i < count; i++)
    {

        switch (option)
        {
        case CHANGE:
        {
            int pre, post;
            uint8_t again = 0;
            int fail_count = 0;

            while (1)
            {
                //lock 'operating on account X' flag
                pthread_mutex_lock(&mutexes[source]);
                // if(again)
                // printf("Account %d value change: trying again...\n", source);

                pre = account_value[source];
                // sem_getvalue(&semaphores[source], &pre);
                //result is positive or adding money to the account?
                if (pre + value > 0 || value > 0)
                    break;

                // printf("Account %d value change debt warning! Waiting for income instead...", source);
                // printf("\t%d %c %d = %d bolívar soberano\n", pre, isNegative ? '-' : '+', absValue, pre+value);
                pthread_mutex_unlock(&mutexes[source]);
                usleep(rand_r(&rand_seed[id]) % MUTEX_RELOCK_DELAY_US);
            }

            account_value[source] += value;
            // sem_post(&semaphores[source])
            post = account_value[source];
            pthread_mutex_unlock(&mutexes[source]);

            printf("Account %d value change: %d %c %d = %d bolívar soberano\n", source, pre, isNegative ? '-' : '+', absValue, post);
            break;
        }
        case MOVE:
        {
            int pre[2], post[2];
            uint8_t again = 0;
            while (1)
            {
                //lock 'operating on account X' flag
                pthread_mutex_lock(&mutexes[source]);

                //lock 'operating on account Y' flag
                while (pthread_mutex_trylock(&mutexes[target]))
                {
                    //target mutex unobtainable:
                    //unlock and lock again source mutex, maybe someone else wants to do stuff on it
                    pthread_mutex_unlock(&mutexes[source]);
                    usleep(rand_r(&rand_seed[id]) % MUTEX_RELOCK_DELAY_US);
                    pthread_mutex_lock(&mutexes[source]);
                }
                //both mutexes acquired now
                if (again)
                    // printf("Account %d to %d transaction: trying again...\n", source, target);

                    //operation will result in debt on one of accounts? release both flags and wait
                    // sem_getvalue(&semaphores[source], &pre[0]);
                    // sem_getvalue(&semaphores[target], &pre[1]);
                    pre[0] = account_value[source];
                pre[1] = account_value[target];
                //result is positive or adding money to the account? Go forward
                if (pre[0] - value > 0 && pre[1] + value > 0)
                    break;

                // printf("Account %d to %d transaction debt warning! Waiting for income instead...\n", source, target);
                again = 1;
            }

            account_value[source] -= value;
            account_value[target] += value;
            post[0] = account_value[source];
            post[1] = account_value[target];

            printf("Account transaction:\n");
            printf("\tAccount %d: %d %c %d = %d bolívar soberano\n", source, pre[0], isNegative ? '+' : '-', absValue, post[0]);
            printf("\tAccount %d: %d %c %d = %d bolívar soberano\n", target, pre[1], isNegative ? '-' : '+', absValue, post[1]);
            break;
        }
        }
    }
    
    pthread_exit(NULL);
}

int main()
{

    srand(time(0));

    for (long i = 0; i < THREAD_COUNT; i++)
    {
        //pre-seed thread random seed
        rand_seed[i] = rand() + i;

        //init accounts
        account_value[i] = rand() % MAX_INITIAL_VALUE; // + 1000;
        if (sem_init(&semaphores[i], 0, account_value[i]))
        {
            perror("failed to init semaphore");
            exit(1);
        }

        if (pthread_mutex_init(&mutexes[i], 0))
        {
            perror("failed to init mutex");
            exit(1);
        }
    }

    //create operation threads
    for (long i = 0; i < THREAD_COUNT; i++)
        if (pthread_create(&threads[i], NULL, &worker_thread, (void *)i) != 0)
        {
            perror("pthread_create failed");
            exit(1);
        }

    pthread_exit(NULL);
}