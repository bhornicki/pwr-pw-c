#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#define PIPE_WR 1
#define PIPE_RD 0

const char const FIFO_NAME[] = "/tmp/lab3_zad2_fifo_hornicki";
const char *gFileName;

const char const CMD_PAUSE[] = "pause\n";
const char const CMD_SPEED_PLUS[] = "speed_incr +0.1\n";
const char const CMD_SPEED_MINUS[] = "speed_incr -0.1\n";
const char const CMD_SPEED_RESET[] = "speed_set 1.0\n";
const char const CMD_VOLUME_UP[] = "volume +20\n";
const char const CMD_VOLUME_DOWN[] = "volume -20\n";
const char const CMD_SEEK_FORWARD5[] = "seek +5.0\n";
const char const CMD_SEEK_BACK5[] = "seek -5.0\n";
const char const CMD_QUIT[] = "quit\n";

//arg1 executable path (silently passed)
//arg2 video file to watch
int main(int argc, char *argv[])
{
	/*
	 * Parse arguments
	 */

	if (argc > 2)
	{
		printf("Invalid arguments\nUse ./zad2 file_to_watch\n");
		return 1;
	}

	if (argc == 2)
		gFileName = argv[1];

	uint8_t isParent = fork();
	if (isParent)
	{
		/*
		 * [P]arent
		 *
		 * Create FIFO
		 */

		if ((access(FIFO_NAME, F_OK) == 0) && remove(FIFO_NAME))
		{
			printf("[P] Failed to remove old FIFO!\n");
			return 2;
		}

		if (mkfifo(FIFO_NAME, 0666))
		{
			printf("[P] Failed to create FIFO!\n");
			return 1;
		}
		printf("[P] FIFO \"%s\" created\n", FIFO_NAME);

	}
	else
	{
		/*
		 * [C]hild
		 */
		sleep(1);
		printf("[C] Starting mplayer..\n");
		char buf[256];
		sprintf(buf, "mplayer -slave -input file=%s %s",FIFO_NAME, gFileName);
		FILE *handle = popen(buf,"w");

		int file = open(FIFO_NAME, O_WRONLY);
		if (file == -1)
		{
			printf("[C] Failed to open FIFO!\n");
			return 1;
		}
		
		
		//write commands
		printf("Command list:\n\n");

		printf("\tp\tPlay/Pause\n\n");

		printf("\ts\tSpeed -0.1x\n");
		printf("\td\tSpeed +0.1x\n");
		printf("\tr\tSpeed reset\n");

		printf("\t[\tSeek 5 sec backwards\n");
		printf("\t]\tSeek 5 sec forward\n\n");

		printf("\tx\tVolume down\n");
		printf("\tc\tVolume up\n\n");

		printf("\tq\tQuit\n\n");
		// main loop
		while (1)
		{
			char c = tolower(getchar());
			switch (c)
			{
			case 'p':
				write(file, CMD_PAUSE, sizeof(CMD_PAUSE));
				break;

			case 's':
				write(file, CMD_SPEED_MINUS, sizeof(CMD_SPEED_MINUS));
				break;
			case 'd':
				write(file, CMD_SPEED_PLUS, sizeof(CMD_SPEED_PLUS));
				break;
			case 'r':
				write(file, CMD_SPEED_RESET, sizeof(CMD_SPEED_RESET));
				break;

			case '[':
				write(file, CMD_SEEK_BACK5, sizeof(CMD_SEEK_BACK5));
				break;
			case ']':
				write(file, CMD_SEEK_FORWARD5, sizeof(CMD_SEEK_FORWARD5));
				break;

			case 'x':
				write(file, CMD_VOLUME_DOWN, sizeof(CMD_VOLUME_DOWN));
				break;
			case 'c':
				write(file, CMD_VOLUME_UP, sizeof(CMD_VOLUME_UP));
				break;

			case 'q':
				write(file, CMD_QUIT, sizeof(CMD_QUIT));
				pclose(handle);
				close(file);
				return 0;
			}
		}
	}
}