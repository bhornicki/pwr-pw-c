#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>

#define PIPE_WR 1
#define PIPE_RD 0

const size_t gReadBlockSize = 4096;
const size_t gWriteBlockSize = 40960;
const size_t gDataSize = 512000;
const size_t gReadDelay = 2; //seconds
size_t gIterationCount = 20;

const char gFifoName[] = "/tmp/lab3_zad1_fifo_hornicki";

//arg1 executable path (silently passed)
//arg2 iteration count
int main(int argc, char *argv[])
{
	/*
	 * Parse arguments
	 */

	if (argc > 2)
	{
		printf("Invalid arguments\nUse ./zad1 iteration_count\niteration_count is by default set to %zd and can be omitted", gIterationCount);
		return 1;
	}

	if (argc == 2)
	{
		gIterationCount = atoi(argv[1]);
	}

	printf("Using %zd iterations\n", gIterationCount);

	/*
	 * Create FIFO
	 */

	if ((access(gFifoName, F_OK) == 0) && remove(gFifoName))
	{
		printf("Failed to remove old FIFO!\n");
		return 2;
	}

	if (mkfifo(gFifoName, 0666))
	{
		printf("Failed to create FIFO!\n");
		return 1;
	}
	printf("FIFO \"%s\" created\n", gFifoName);

	uint8_t isParent = fork();
	if (isParent)
	{
		/*
		 * [P]arent
		 */
		int file = open(gFifoName, O_RDONLY);
		if (file == -1)
		{
			perror("[P] Failed to open FIFO!\n");
			return 1;
		}

		char buffer[gReadBlockSize];

		for (size_t i = 0; i < gIterationCount; i++)
		{
			sleep(gReadDelay);

			int len = read(file, buffer, gReadBlockSize);
			if (len < 0)
			{
				printf("[P %zd] Read failed!\n",i);
				close(file);
				return 1;
			}
			printf("[P %zd] Received %d bytes\n", i, len);
		}

		close(file);
	}
	else
	{
		/*
		 * [C]hild
		 */

		int file = open(gFifoName, O_WRONLY);
		if (file == -1)
		{
			printf("[P] Failed to open FIFO!\n");
			return 1;
		}

		//prepare message
		char buffer[gDataSize];
		for (size_t i = 0; i < gDataSize - 1; i++)
			buffer[i] = 'A' + (random() % 26);
		buffer[gDataSize - 1] = 0;

		size_t offset = 0;
		for (size_t i = 0; i < gDataSize; i++)
		{
			int size = gDataSize - offset;
			if (size > gWriteBlockSize)
				size = gWriteBlockSize;
			
			int len = write(file, buffer + offset, size);
			offset += size;
			if (len < 0)
			{
				printf("[C %zd] Write failed!\n", i);
				close(file);
				return 1;
			}
			printf("[C %zd] Transmitted %d bytes\n", i, len);
			if(offset >= gDataSize)
				break;
		}
		printf("[C] Finished transmission\n");
		close(file);
	}
}