#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    uint8_t isParent = fork();

    if (isParent)
        printf("Parent:\n");
    else
        printf("Child:\n");

    printf("\tPID: %d\n\tPPID:%d\n", getpid(), getppid());
    return 0;
}