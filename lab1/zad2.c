#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
    
    int p[2];
    //-1 when pipe creation has failed
    if (pipe(p) < 0)
    {
        printf("Failed to create pipe\n");
        return 1;
    }

    uint8_t isParent = fork();
    if (isParent)
    {
        // parent
        char message[] = "Bartosz Hornicki";
        write(p[1], message, strlen(message));
    }
    else
    {
        // child
        sleep(1);
        char buffer[32] = {};
        int len;

        //print received data in 32 bytes chunks.
        //-1 for error, 0 if no data was received,
        while ((len = read(p[0], buffer, 32)) > 0)
            printf("%s\n", buffer);

        if (len != 0)
            return 2;
    }

    return 0;
}