#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

int main(int argc, char *argv[])
{

    int mosi[2]; //master (parent) out, slave (child) in
    int miso[2]; //master in, slave out

    //-1 when pipe creation has failed
    if ((pipe(mosi) < 0) || (pipe(miso) < 0))
    {
        printf("Failed to create pipes\n");
        return 1;
    }

    uint8_t isParent = fork();
    if (isParent)
    {
        // parent
        close(mosi[0]);
        close(miso[1]);

        char buffer[128] = "";

        printf("Podaj polecenie: ");
        fgets(buffer, 128, stdin);
        write(mosi[1], buffer, strlen(buffer));

        while (1)
        {
            int len;
            while ((len = read(miso[0], buffer, 128)) <= 0)
                ;
            if (len < 0)
                return 2;
            printf("[print parent]: %s\n", buffer);
        }
    }
    else
    {
        // child
        close(miso[0]);
        close(mosi[1]);
        char buffer[128] = {};

        // sleep(5);

        dup2(miso[1], STDOUT_FILENO);
        close(miso[1]);

        int len;
        while ((len = read(mosi[0], buffer, 128)) <= 0)
            ;
        if (len < 0)
            return 2;

        buffer[len - 1] = 0; //replace /n
        printf("[print child]: received %s", buffer);
        fflush(stdout);

        execl(buffer, buffer, NULL);
    }

    return 0;
}