#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <unistd.h>
#include <ctype.h>

#define ACCOUNT_MAX_COUNT 64

typedef struct SharedMemory
{
    int cnt;
    sem_t semaphore;
    int buf[ACCOUNT_MAX_COUNT];
} SharedMemory;

//I miss cpp enum class with underlaying type enforcement
enum Options
{  
    CREATE_CLOSE = 2,
    READ = 3,
    CHANGE,
    MOVE
};

int main(int argc, char *argv[])
{

    key_t key = ftok(argv[0], 65);
    int shmid;
    SharedMemory *mem;

    int option, source, value, target, count;
    option = argc;
    
    /*
     * process arguments
     */
    switch (option)
    {
    case CREATE_CLOSE:
    {
        char c = tolower(argv[1][0]);
        if( c == 'c')
        {
            printf("force-closing shared memory\n");
            
            shmid = shmget(key, sizeof(SharedMemory), 0666 | IPC_CREAT);
            shmctl(shmid,IPC_RMID,NULL);
            exit(0);
        }
        else if (c == 'o')
        {
            shmid = shmget(key, sizeof(SharedMemory), 0666 | IPC_CREAT | IPC_EXCL);
            if(shmid == -1)
            {
                perror("create shared memory");
                exit(1);
            }
            mem = (SharedMemory *)shmat(shmid, (void *)0, 0);
            mem->cnt = 0;
            //"Attempting to initialise an already initialised semaphore results in undefined behaviour"
            sem_init(&mem->semaphore, 1, 1);
            exit(0);
        }
        else
        {
            printf("invalid argument\n");
            printf("\tpass 'c' to force-close shared memory\n");
            printf("\tpass 'o' to open/create shared memory\n");
            exit(1);
        }
    }
    case READ:
        source = atoi(argv[1]);
        count = atoi(argv[2]);
        break;
    case CHANGE:
        source = atoi(argv[1]);
        value = atoi(argv[2]);
        count = atoi(argv[3]);
        break;
    case MOVE:
        source = atoi(argv[1]);
        target = atoi(argv[2]);
        value = atoi(argv[3]);
        count = atoi(argv[4]);
        if (target < 0 || target >= ACCOUNT_MAX_COUNT)
        {
            perror("invalid target");
            exit(option);
        }
        break;

    default:
        perror("invalid argument count");
        exit(1);
    }

    if (source < 0 || source >= ACCOUNT_MAX_COUNT)
    {
        perror("invalid source");
        exit(option);
    }
    if (count <= 0)
    {
        perror("invalid operation count");
        exit(option);
    }

    /*
     * create or join shared memory
     */

    //create shared memory
    shmid = shmget(key, sizeof(SharedMemory), 0666);
    if (shmid == -1)
    {
        perror("connect to shmem");
        exit(1);
    }

    mem = (SharedMemory *)shmat(shmid, (void *)0, 0);
    

    //join "attendance list"
    sem_wait(&mem->semaphore);
    mem->cnt++;
    sem_post(&mem->semaphore);

    //do something
    for (int i = 0; i < count; i++)
    {
        sem_wait(&mem->semaphore);
        switch (option)
        {
        case READ:
            //bolívar soberano? Woah, that's worthless!
            printf("Account %d status: %d bolívar soberano\n", source, mem->buf[source]);
            break;
        case CHANGE:
        {
            int pre, post;
            pre = mem->buf[source];
            mem->buf[source] += value;
            post = mem->buf[source];

            printf("Account %d value change: %d + %d = %d bolívar soberano\n", source, pre, value, post);
            break;
        }
        case MOVE:
        {
            int pre[2], post[2];
            pre[0] = mem->buf[source];
            pre[1] = mem->buf[target];
            mem->buf[source] -= value;
            mem->buf[target] += value;
            post[0] = mem->buf[source];
            post[1] = mem->buf[target];

            printf("Account transaction:\n");
            printf("\tAccount %d: %d - %d = %d bolívar soberano\n", source, pre[0], value, post[0]);
            printf("\tAccount %d: %d + %d = %d bolívar soberano\n", target, pre[1], value, post[1]);
            break;
        }
        }
        sem_post(&mem->semaphore);
        usleep(1000);
    }

    //leave "attendance list"
    uint8_t lastOne;
    sem_wait(&mem->semaphore);
    mem->cnt--;
    lastOne = !mem->cnt;
    sem_post(&mem->semaphore);

    //leave and/or delete shared memory
    shmdt(mem);
    // if(lastOne)
    // {
        // sem_close(&mem->semaphore);
        // shmctl(shmid,IPC_RMID,NULL);
    // }


    return 0;
}