#!/usr/bin/env python3

import sys
import subprocess
import random

ACCOUNT_MAX_COUNT = 64  # must match #define in zad1.c
REPEAT_COUNT = 5  # argument passed to zad1


def main():
    #
    # process arguments
    #

    if len(sys.argv) != 2:
        print(f"invalid argument count! Use {sys.argv[0]} account_count")
        exit(1)

    try:
        count = int(sys.argv[1])
    except ValueError:
        print(f"cannot convert account_count to numeric value!")
        exit(1)

    if count >= ACCOUNT_MAX_COUNT:
        print(f"invalid account count! Pass account_count value in range 0..{ACCOUNT_MAX_COUNT-1}")
        exit(1)

    #
    # process maker
    #
    random.seed()

    print(f"Calling shared memory creator")
    subprocess.call(f"./zad1 o", shell=True)

    print(f"Running with {count} accounts")
    print("------------------------------")

    # create concurrent processes
    pid = []
    for i in range(count):
        #move
        pid.append(subprocess.Popen(f"./zad1 {i} {random.randrange(count)} {random.randint(-50,50)} {REPEAT_COUNT}", shell=True))   
        #change
        pid.append(subprocess.Popen(f"./zad1 {i} {random.randint(-50,50)} {REPEAT_COUNT}", shell=True))
        #read
        pid.append(subprocess.Popen(f"./zad1 {i} {REPEAT_COUNT}", shell=True))

    #wait for all tasks to complete
    for i in pid:
        i.wait()
    
    print("------------------------------")
    print(f"Closing shared memory")
    subprocess.call(f"./zad1 c", shell=True)


if __name__ == "__main__":
    main()