#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <math.h>

#define PIPE_WR 1
#define PIPE_RD 0

int *gPipes = NULL;
pid_t *gThreads = NULL;

int main(int argc, char *argv[])
{
    /*
	 * Parse arguments
	 */

    if (argc == 1)
    {
        printf("Use ./zad2 function1_path function1_argument1 function1_argument2 function1_argumentN , ");
        printf("function2_path function2_argument1 function2_argument2 function2_argumentN , ");
        printf("functionN_path functionN_argument1 functionN_argument2 functionN_argumentN\n");
        printf("space before and after comma is mandatory!");
        return 1;
    }

    /*
     * 1. Count functions
     */
    size_t commandCount = 1;
    for (size_t i = 0; i < argc; i++)
        if (argv[i][0] == ',')
            commandCount++;

    printf("found %zd commands\n", commandCount);

    /*
     * 2. Prepare pipes
     */

    if (commandCount > 1)
    {
        gPipes = malloc(sizeof(int) * 2 * (commandCount - 1));
        if (!gPipes)
        {
            printf("Failed to allocate memory for pipes!\n");
            return 1;
        }
        for (size_t i = 0; i < commandCount - 1; i++)
        {
            if (pipe(gPipes + i * 2) < 0)
            {
                printf("Failed to create %zd pipe!", i);
                return 1;
            }
        }
    }
    /*
     * 3. Prepare threads
     */
    gThreads = malloc(sizeof(pid_t) * commandCount);
    if (!gThreads)
    {
        printf("Failed to allocate memory for thread PIDs!\n");
        return 1;
    }

    /*
     * 3. run threads
     */

    for (size_t i = 0; i < commandCount; i++)
    {
        size_t commandOffset = 1;
        //prepare command and arguments
        char *command = argv[commandOffset];
        char *args = NULL;

        //TODO: iterate between commas
        //TODO: create array with pointers to: {argc[0], argument1, argument2, argumentN, NULL}
        // or rather instead of argc[0] pass something like file buffer?
        size_t lastArg = commandOffset + 1;
        while (lastArg <= argc && argv[lastArg][0] != ',')
            lastArg++;
        if (lastArg <= argc)
            args = argv[2];

        
        //create process
        int pid = fork();
        if (pid)
        {
            //parent
            gThreads[i] = pid;
            continue;
        }

        //child
        //TODO
        //variant 1: only one command
        //variant 2: first command (stdout to pipe out)
        //variant 3: middle command (pipe in to exec, stdout to pipe out)
        //variant 4: middle command (pipe in to exec)
        int pipeIn = 0;
        int pipeOut = 0;
        if (commandCount > 1)
        {
            if (i != 0)
                pipeIn = i * 2 + PIPE_RD;

            if (i != commandCount)
                pipeOut = (i + 1) * 2 + PIPE_WR;

            //reroute STDOUT to pipeOut
            dup2(pipeOut, STDOUT_FILENO);
        }

        //TODO: read pipe in
        char buffer[512] = "";
        int len = 0;
        if (i > 0)
        {
            len = read(pipeIn, buffer, 512);
            if (len < 0)
                return 1;
        }

        //TODO: execute command
        execv(command, args);
    }

    /*
     * 4 & 6. wait for all threads to finish
     */
    for (size_t i = 0; i < commandCount; i++)
    {
        waitpid(gThreads[i], NULL, 0);
    }

    free(gThreads);

    /*
     * 5. free pipes array memory
     */
    free(gPipes);

    /*
     * 7. exit
     */
    return 0;
}