# 248872
Hornicki Bartosz Tomasz

Programowanie Współbieżne, dr inż. Jędrzej Ułasiewicz <!-- <3 -->

# lab1
## zad1
Forks

Print PID of a parent and a child

## zad2
Pipes

Send message from parent to the child and print it from the child process

## zad3
Dup (allocate new file descriptor), exec

Pipe redirection. Parent sends a command to the child. Child executes the command, parent receives the output.

# lab2
## zad1
FIFO

Child writes data into a FIFO, server reads it.

## zad2
Pipes & forking

Create program that works like Linux | pipe. Commands are passed as arguments and `,` argument pipes the output of the previous command to the input of the next one.
Code is probably not finished.

# lab3
## zad1
similar to lab2 zad1, whild writes data into a fifo, but in multiple chunks, with delay in between. Server receives and prints the data.

## zad2
popen, fifo
CLI for mplayer - open mplayer video player, connect to its fifo file used for media control. Send commands for seek, modify volume and playback speed based on which key/character was typed into the command line input.

## lab4
semaphore, shared memory - shmget, ftok...

Accounts. Read, change and move value from one account to another. Accounts are stored in a shared memory block, guarded by one semaphore. 
Python script popen's the compiled C code. Each account variable gets three instances of a program: one to transfer value to another account, one to change value and one to display current value. Program repeats the operation multiple times. After all the programs are completed, the shared memory is removed with another popen call.

## lab5
pthread
Similar to lab4, but each account has its own semapore. Each oeration type has its own pthread.